import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Articulo } from 'src/app/shared/entity/articulo.type';
import { CestaService } from 'src/app/shared/service/http/cesta.service';
import { SessionService } from 'src/app/shared/service/session/session.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {

  send=false;
  dataList:Articulo[]=[];  
  form!:FormGroup
  dataFile!:any;

  constructor(
    private readonly svcCesta: CestaService,
    private readonly svcSession: SessionService
  ) { }

  ngOnInit(): void {
    this.getArticulos()
  }

  async getArticulos(){
    try{
      const idUser = this.svcSession.getKeySession("id");
      this.dataList = await this.svcCesta.getByUser(idUser);
    }catch(e){
      console.log(e)
    }

  }

  validFile(imgStr:string){
    if(!imgStr.search('data:image/')){
      return imgStr;
    }
    return '';
  }

  async delete(id:number){
    try{
      const idUser = this.svcSession.getKeySession("id");
      await this.svcCesta.delete(idUser, id)
      this.getArticulos()
    }catch(e){
      console.log(e)
    }
  }

}
