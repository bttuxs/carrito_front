import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tienda } from 'src/app/shared/entity/tienda.type';
import { TiendaService } from 'src/app/shared/service/http/tienda.service';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.scss']
})
export class TiendasComponent implements OnInit {
  send = false;
  tiendaList:Tienda[] =[];
  formTienda!: FormGroup;
  constructor(private readonly svcTienda:TiendaService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.getTiendas()
    this.make()
  }

  async getTiendas(){
    this.send = true
    try{
      const data = await this.svcTienda.getTiendas();
      this.tiendaList = data
      this.send = false;
    }catch(e){
      console.log(e)
      this.send = false;
    }
  }

  make(){
    this.formTienda = this.formBuilder.group({
      sucursal:['', Validators.required],
      direccion: ['', Validators.required]
    });
  }

  async createTienda(){
    this.send = true    
    try{
      const tienda = this.formTienda.value
      await this.svcTienda.createTienda(tienda);
      await this.getTiendas();
      this.formTienda.reset()
      this.send = false
    }catch(e){
      console.log(e)
      this.send = false
    }
  }


}
