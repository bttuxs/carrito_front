import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { cliente } from 'src/app/shared/entity/cliente.type';
import { ClientesService } from 'src/app/shared/service/http/clientes.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  dataList:cliente[]=[];
  form!:FormGroup;
  send=false
  constructor(private readonly svcClientes:ClientesService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.getdata();
    this.make();
  }

  async getdata(){
    try{
      const data = await this.svcClientes.get();
      this.dataList = data
    }catch(e){
      console.log(e)
    }
  }

  make(){
    this.form = this.formBuilder.group({
      nombre:['', Validators.required],
      apellidos: ['', Validators.required],
      direccion: ['', Validators.required]
    });
  }

  async createTienda(){
    this.send = true    
    try{
      const data = this.form.value
      await this.svcClientes.create(data)
      await this.getdata();
      this.form.reset()
      this.send = false
    }catch(e){
      console.log(e)
      this.send = false
    }
  }

}
