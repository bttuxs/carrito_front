import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Articulo } from 'src/app/shared/entity/articulo.type';
import { ArticulosService } from 'src/app/shared/service/http/articulos.service';
import { CestaService } from 'src/app/shared/service/http/cesta.service';
import { SessionService } from 'src/app/shared/service/session/session.service';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.scss']
})
export class ArticulosComponent implements OnInit {

  send=false;
  dataList:Articulo[]=[];  
  form!:FormGroup
  dataFile!:any;
  constructor(
    private readonly formBuilder:FormBuilder,
    private svcArticulo:ArticulosService,
    private readonly svcSession: SessionService,
    private readonly svcCesta: CestaService) { }

  ngOnInit(): void {
    this.makeForm()
    this.getArticulos()
  }

  async getArticulos(){
    try{
      this.dataList = await this.svcArticulo.getArticulos()
    } catch(e){
      console.log(e)
    }   
  }

  makeForm(){
    this.form = this.formBuilder.group({
      codigo: ['', Validators.required],
      descripcion: ['', Validators.required],
      precio: ['', Validators.required],
      imagen: [''],
      stock: ['']
    })
  }

  async create(){
    try{
      const article = {
        ...this.form.value,
        imagen: this.dataFile
      }
      console.log(article)
      let result = await this.svcArticulo.create(article)
      this.form.reset()
      this.getArticulos()
      console.log(result)
    }catch(e){
      console.log(e)
    }

  }

  file(file:any) {
    const reader = new FileReader();
    reader.readAsDataURL(file.target.files[0]);
    reader.onload = () => {this.dataFile = reader.result};
  }

  validFile(imgStr:string){
    if(!imgStr.search('data:image/')){
      return imgStr;
    }
    return '';
  }

  async addToCar(id: number){
    const idUser:number = this.svcSession.getKeySession("id")
    const dataCar = {
      id_articulo: id,
      id_cliente: idUser
    }
    try{
      const cesta = await this.svcCesta.create(dataCar);
      console.log(cesta)
    }catch(e){
      console.log(e)
    }


  }

}
