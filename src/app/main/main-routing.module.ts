import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { TiendasComponent } from './tiendas/tiendas.component';
import { ClientesComponent } from './clientes/clientes.component';
import { CarritoComponent } from './carrito/carrito.component';

const routes: Routes = [
  {path:"", component: MainComponent, 
  children: [
    {path:'', component: ArticulosComponent},
    {path:'tienda', component: TiendasComponent},
    {path:'clientes', component: ClientesComponent},
    {path:'carrito', component: CarritoComponent},
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
