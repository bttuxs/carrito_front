import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { MainComponent } from './main.component';
import { SharedModule } from '../shared/shared.module';
import { ArticulosComponent } from './articulos/articulos.component';
import { TiendasComponent } from './tiendas/tiendas.component';
import { ClientesComponent } from './clientes/clientes.component';
import { CarritoComponent } from './carrito/carrito.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MainComponent,
    ArticulosComponent,
    TiendasComponent,
    ClientesComponent,
    CarritoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MainRoutingModule
  ]
})
export class MainModule { }
