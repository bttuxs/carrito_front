export type Cesta = {
    id: number;
    id_articulo: number;
    id_cliente: number;
    created_at:Date
}
export type CestaCreate = {
    id_articulo: number;
    id_cliente: number;
}