export type Usuario = {
    id: number;
    usuario: string;
    password: string;
    id_cliente: number;
    created_at: Date;
}