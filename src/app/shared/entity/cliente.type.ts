export type cliente = {
    id: number;
    nombre: string;
    apellidos: string;
    direccion: string;
}