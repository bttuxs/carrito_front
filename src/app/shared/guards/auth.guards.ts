import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionService } from '../service/session/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _router: Router, private readonly svcSession: SessionService){}

  async canActivate() {
    try {
        console.log(this.svcSession.isLogged())
      if(this.svcSession.isLogged()){
        return true;
      }else{
        this._router.navigate(['/']);
        return false;
      }
    } catch (e) {
      this._router.navigate(['/']);
      return false;
    }
  }
  
}
