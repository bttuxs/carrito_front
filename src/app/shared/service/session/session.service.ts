import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private nameSession = "sessionUser";
  constructor(
    private router: Router
    ) { }

  setNameSession(name: string) {
    this.nameSession = name;
    return this;
  }

  setDataSession(user:any) {
    localStorage.setItem(this.nameSession, JSON.stringify(user))
    return this;
  }

  getAllData() {
    const data = localStorage.getItem(this.nameSession)
    if(data){
        return JSON.parse(data);
    }
    return null
  }

  getKeySession(key:string) {
    let data = this.getAllData();
    if (data != null) {
      if (data.hasOwnProperty(key)) {
        return data[key];
      }
    }
    return null;
  }

  setKeySession(key:string, value:string) {
    let data = this.getAllData();
    if (!data) {
      data = [];
    }
    data[key] = value;
    this.setDataSession(data)
    return this;
  }

  clearSession() {
    localStorage.removeItem(this.nameSession);
    return this;
  }

  clearAllSession() {
    localStorage.clear();
    return this;
  }

  isLogged() {
    let token = this.setNameSession(this.nameSession).getKeySession("id");
    if ( !token ) {
      return false;
    }
    return true;
  }

  closeSession() {
    this.clearSession()
    this.router.navigate(['/auth'])
  }
}
