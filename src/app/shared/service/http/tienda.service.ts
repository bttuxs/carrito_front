import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { firstValueFrom } from 'rxjs';
import { Tienda } from '../../entity/tienda.type';
@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  constructor(
    private httpSvc:HttpService
  ) { }

  getTiendas(){
    const path = "api/Tienda"
    return firstValueFrom(this.httpSvc.get(path))
  }

  createTienda(tienda:Tienda){
    const path = "api/Tienda"
    return firstValueFrom(this.httpSvc.post(path, tienda))
  }

}
