import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { firstValueFrom } from 'rxjs';
import { Usuario } from '../../entity/usuario.type';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private httpSvc:HttpService
  ) { }

  get(){
    const path = "api/Usuario"
    return firstValueFrom(this.httpSvc.get(path))
  }

  login(usuario:Usuario){
    const path = "api/Usuario/login"
    return firstValueFrom(this.httpSvc.post(path, usuario))
  }

  create(usuario:Usuario){
    const path = "api/Usuario"
    return firstValueFrom(this.httpSvc.post(path, usuario))
  }

}
