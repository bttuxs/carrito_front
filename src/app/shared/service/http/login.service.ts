import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpSvc:HttpService
  ) { }

  login(user:string, pass:string){
    const path = "login/auth"
    const params = { 
      user: user,
      pass: pass
    }
    return this.httpSvc.post(path, params).toPromise()
  }

}
