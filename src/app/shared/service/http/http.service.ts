import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public httpOptions: any;
  public httpHeaders: HttpHeaders;
  private urlBase = environment.dominio;

  constructor(
    private httpClient: HttpClient
  ) {
    this.httpHeaders = new HttpHeaders();
  }

  loadHeaders() {
    this.httpOptions = {
      headers: this.httpHeaders
    };
    return this.httpOptions;
  }

  setHeader(key: string , value: string) {
    this.httpHeaders = this.httpHeaders.set(key, value);
  }

  setUrlBase(path?:string) {
    this.urlBase = path || environment.dominio ;
    return this;
  }

  getUrlBase() {
    return this.urlBase;
  }

  get(pathUrl:string, headers = {}): Observable<any> {
    let url = this.getUrlBase() + pathUrl;
    return this.httpClient.get(url, headers).pipe(resp => resp);
  }

  post(pathUrl:string, params:any, headers = {}): Observable<any> {
    let url = this.getUrlBase() + pathUrl;
    return this.httpClient.post(url, params, headers).pipe(resp => resp);
  }

  patch(pathUrl:string, params:any, headers = {}): Observable<any> {
    let url = this.getUrlBase() + pathUrl;
    return this.httpClient.patch(url, params, headers).pipe(resp => resp);
  }

  put(pathUrl: string, params: any, headers = {}): Observable<any> {
    let url = this.getUrlBase() + pathUrl;
    return this.httpClient.put(url, params, headers).pipe(response => response);
  }

  delete(pathUrl:string, params:any, headers :any= {}): Observable<any> {
    let url = this.getUrlBase() + pathUrl;
    headers['body'] = params;
    return this.httpClient.delete(url, headers).pipe(resp => resp);
  }

  objToQueryString(obj: any) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }
}
