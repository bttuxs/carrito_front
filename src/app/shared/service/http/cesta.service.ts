import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { CestaCreate } from '../../entity/cesta.entity';
import { firstValueFrom } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CestaService {

  constructor(
    private httpSvc:HttpService
  ) { }

  create(articulo:CestaCreate){
    const path = "api/Cesta"
    return firstValueFrom(this.httpSvc.post(path, articulo))
  }

  getByUser(user:number){
    const path = "api/Cesta/" + user
    return firstValueFrom(this.httpSvc.get(path))
  }

  delete(idUser: number, idItem:number){
    const path = "api/Cesta/"+ idUser+"/" + idItem
    return firstValueFrom(this.httpSvc.delete(path, {}))
  }

}
