import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { firstValueFrom } from 'rxjs';
import { Tienda } from '../../entity/tienda.type';
@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(
    private httpSvc:HttpService
  ) { }

  get(){
    const path = "api/Cliente"
    return firstValueFrom(this.httpSvc.get(path))
  }

  create(tienda:Tienda){
    const path = "api/Cliente"
    return firstValueFrom(this.httpSvc.post(path, tienda))
  }

}
