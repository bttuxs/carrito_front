import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { firstValueFrom } from 'rxjs';
import { Tienda } from '../../entity/tienda.type';
@Injectable({
  providedIn: 'root'
})
export class ArticulosService {

  constructor(
    private httpSvc:HttpService
  ) { }

  getArticulos(){
    const path = "api/Articulo"
    return firstValueFrom(this.httpSvc.get(path))
  }

  create(tienda:Tienda){
    const path = "api/Articulo"
    return firstValueFrom(this.httpSvc.post(path, tienda))
  }

}
