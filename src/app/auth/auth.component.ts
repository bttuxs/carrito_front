import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../shared/service/http/usuario.service';
import { SessionService } from '../shared/service/session/session.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public formLogin!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router:Router,
    private svcUsuario:UsuarioService,
    private readonly svcSession: SessionService
  ) { }

  ngOnInit(): void {
    this.makeForm();
  }

  makeForm(){
    this.formLogin = this.formBuilder.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  async login(){
    try{
      const data = await this.svcUsuario.login(this.formLogin.value)
      if(data.id)
      {
        this.svcSession.setDataSession(data);
        this.router.navigate(["/main"])
      }
    }catch(e){
      console.log(e)
    }
  }

}
