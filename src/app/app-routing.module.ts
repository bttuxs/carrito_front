import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared/guards/auth.guards';

const routes: Routes = [
  {path: "", component: AppComponent,
  children:[
    {
      path: "", loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule)
    }
  ]},
  {path: "main",
  children:[
    {
      path: "", loadChildren: () => import('./main/main.module').then((m) => m.MainModule)
    }
  ],
  canActivate: [AuthGuard],}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
